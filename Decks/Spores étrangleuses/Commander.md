### Créatures (36) dont 20 fongus

Fongepied, le clandestin (C)

Araignée pêche-ciel
Archer aux pointes empoisonnées
Aruspice thallidé
Bête pestegueule
Boa sinueux
Carcasse de floraison
Chancreflore
Dina, macéreuse d'âmes
Ermite thélonite
Force verdoyante
Jeune calotte de la mort
Mage du jade
Mycon d'utopie
Myconide du Cellier
Némata, gardienne primitive
Opportuniste morbide
Rampeur à spores
Rôdeur de boisblanc
Sage de l'évolution
Saproberger de la Yavimaya
Sporoloss ancien
Tertre aux spores
Thallidé à fleurs de mort
Thallidé encoquillé
Thallidé faucheur de fléau
Thallidé fritillaire
Thallidé germinateur
Thallidé mortespore
Thallidé omnivore
Thallidé psychotrope
Thallidé tukalanguier
Thélon de Havrebois
Tisseuse de sporetoile
Titubeur à rhizomes
Veilleur de spores myconide


### Éphémères (10)

Bourgeonnement
Carnage de la forêt
Charme de Golgari
Courage résonnant
Essaim de bourgeons
Essaim de spores
Feuillage immédiat
Graines en pluie
Infection fongoïde
Renaissance fongoïde


### Rituels (10)

Chasse à la bête
Floraison morbide
Fragments d'os
Imagination fertile
Migration de saprobiontes
Mutation mortelle
Noyade dans la sanie
Paillis
Regard de granit
Restauration surnaturelle


### Enchantements (7)

Bastion du souvenir
Étreinte verdoyante
Nécrogénèse
Pépinières de champignons
Poings de ferbois
Reboisement de moiselierre
Survivre à la force des bras


### Artefacts (5)

Anneau solaire
Autel de la démence
Cachet d’ésotérisme
Cachet de Golgari
Sacoche druidique


### Terrains (32)

15 Forêt (dont 8 Golgari)

8 Marais (dont 8 Golgari)

Dépression de jungle
Étendues sauvages en évolution
Ferme à putréfaction des Golgari
Fondrière hantée
Paysage myriadaire
Porte de la guilde de Golgari
Tour de commandement
Verger exotique
Verger infâme


### Jetons (10)

9 Saprobionte
1 Phyrexian et saprobionte
