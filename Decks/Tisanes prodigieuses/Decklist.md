### Créatures (28)

Zimone et Dina

Arpenteur jaillissant
Assistant raccommodé
Brigadier des damnés
Chercheuse en sang
Chimère tremblebois
Consécrateur gargouillant
Dina, macéreuse d'âmes
Dissident damné
Éloïse, détective de Néphalie
Escogriffe fongoïde
Faucheur d’âmes de Mogis
Fermière excentrique
Goule grizzli
Goule piquée à l'argent
Livreur myr
Loutre voleuse
Marionnettiste gixien
Mécanicienne de mécanoptère
Némata, gardienne primitive
Prédicateur de synthèse
Rampeur à spores
Savra, Reine des Golgari
Shamane scintispore
Skaab céleste
Tertre aux spores
Visionnaire de Llanowar
Zimone, prodige de Quandrix


### Rituels (8)

Chuchotements nocturnes
Collet de vide
Concentration
Culture
Espionnage phyrexian
Explosion de spore
Lire dans les os
Paillis


### Éphémères (18)

Abrogation mystique
Changer l'équation
Conviction corrompue
Décomposition abrupte
En plein Roulis
Froideur de la tombe
Gestion
Lance de mortalité
Option
Percée
Puissance des traditions
Putréfier
Remise à neuf
Réprobation
Scruter
Stupéfier
Surprise
Vignes bourgeonnantes


### Artefacts (10)

Babiole de Mishra
Babiole du voyageur
Cachet d'ésotérisme
Cachet de Golgari
Clé d'argent de lune
Grimoire de Jalum
Mine rugissante
Pierre de l’esprit
Prisme prophétique
Sphère du commandant


### Enchantements (2)

Bastion du souvenir
Sceau de primordium


### Batailles (1)

Invasion de Vryn
