### Court terme

- Coffres de la coterie proxy FR 80
- Jeton écureuil proxy 240 + zombie décomposition
- Jeton humain 240 + zombie décomposition
- Jeton zombie bleu/noir X/X avec menace
- plus de jetons zombie
- Jetons copie + zombie décomposition
- Escogriffe putride -> Remords accablants


### Questions

- Remplacer Zombie cannibale -> Scryfall 1 mana zombie ?
- Contaminateur des tombes (filtrage) ?
- Fragments d'os (removal) ?
- Exécuteur réanimé (lord)
- Obscure prophétie (pioche)
- Cor du héraut (artefact lord)
- Champ des morts ? (créateur jetons)
- Takenuma, marais abandonné ? 12 €
- Libérateur des cryptes ?
- Zombie master
- Seigneur des mort-vivants
- Découverte de congénères ? (flavor pb)
- Tutelle féroce ?? 
- Ajouter un contre bleu éphémère ?
- Scryfall Zombie tokens
- Remords accablants ?
