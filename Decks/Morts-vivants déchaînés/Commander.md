### Créatures (40)

Wilhelt, hacheur de pourriture

Adversaire souillé
Alchimiste de Sedraxis
Amalgame surchargé
Archigoule de Thraben
Assassin de la Forteresse
Augure mort-vivant
Baron de la mort
Béhémoth de Morkrut
Cadavre ambulant
Capitaine du diregraf
Cavalier sans tête
Champion des trépassés
Chef de guerre mort-vivant
Colosse du diregraf
Création oubliée
Faucheur de minuit
Fossoyeur de Selhoff
Foule du galgengraf
Goule délétère
Goule sangsucéphale
Horde sans souffle
Josu Vess, chevalier liche
Leveur de goule
Maraudeur sacpeau
Mort vengeur
Porte-étendard de Liliana
Putréventre vorace
Rampeur des tombes
Seigneuresse des maudits
Skaab ailé
Skaab au hachoir
Skaab aux crânes
Skaab maillé de lames
Tyran des tombes
Zombie boiteux
Zombie cannibale
Zombie de siège
Zombie du cellier
Zombie rongeur


### Planeswalkers (1)

Liliana, épargnée par la mort


### Batailles (1)

Invasion d'Innistrad


### Rituels (9)

Apocalypse des zombies
Armée des damnés
Brimades
Maléfice nécrotique
Mélodie lointaine
Obscur salut
Ordre du patriarche
Remords accablants
Vider le laboratoire


### Enchantements (7)

Bastion du souvenir
Interminables colonnes de morts
Nécrodualité
Orage sur le toit
Ouvrez les tombes !
Procession de goules
Reflets de Littjara


### Éphémères (3)

Accumulation
Réparation de cadavre
Retrouvailles pourries


### Artefacts (6)

Anneau solaire
Autel d’Ashnod
Bûcher funéraire des héros
Cachet d’ésotérisme
Cachet de Dimir
Crypte surpeuplée


### Terrains (33)

18 Marais (dont 1 Warhammer)

1 Île (foil full art)

Catacombes de Sombreau
Coffres de la Coterie
Cour retirée
Dépression engloutie
Estuaire asphyxié
Étendues sauvages en évolution
Fondrière mortuaire
Île souillée
Marécage aux épaves
Marigot lugubre
Nykthos, reliquaire de Nyx
Rivière souterraine
Terra nullius
Tour de commandement


### Jetons (14)

5 Zombie / Zombie en décomposition

Zombie bleu / Zombie en décomposition

Humain / Zombie en décomposition

7 Copie / Zombie en décomposition


### Dévotions

68 B (79 %), 18 U (21 %)
